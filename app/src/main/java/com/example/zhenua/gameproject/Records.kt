package com.example.zhenua.gameproject

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_records.*

class Records : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_records)

        val moleContainer = getSharedPreferences(moleFile, Context.MODE_PRIVATE)

        val totalKills = moleContainer.getInt(totalKillsKey,0)
        val bestStreak = moleContainer.getInt(bestStreakKey,0)
        stats.text = "Всего убито кротов: $totalKills\n\n\nЛучшая серия убийств: $bestStreak"


        refreshStats.setOnClickListener()
        {
            val moleContainer = getSharedPreferences(moleFile, Context.MODE_PRIVATE)
            val refreshTotal = moleContainer.edit()
            refreshTotal.putInt(totalKillsKey,0).apply()

            val moleBestContainer = getSharedPreferences(moleFile, Context.MODE_PRIVATE)
            val refreshBest = moleBestContainer.edit()
            refreshBest.putInt(bestStreakKey,0).apply()
        }

        back.setOnClickListener(){ finish() }

    }
    companion object {
        const val totalKillsKey = "com.example.zhenua.gameproject.totalKillsKey"
        const val bestStreakKey = "com.example.zhenua.gameproject.bestStreakKey"
        const val moleFile = "moleContainer"
    }
}
