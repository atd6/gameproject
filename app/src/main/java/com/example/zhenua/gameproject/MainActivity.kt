package com.example.zhenua.gameproject

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

import java.util.*






class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var x: Int
        var y: Int
        //val minX = field.top
        //val maxX = field.height - mole.height
        //val maxY = field.width - mole.width
        //val minY = field.left
        var moleKilled: Int = 0

        object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                var sec =  millisUntilFinished / 1000
                timeControl.text = "Время \n$sec"

                if (sec % 3 == 0L)
                {
                    if (mole.visibility == View.INVISIBLE){
                        mole.visibility = View.VISIBLE
                        mole.isEnabled = true
                        x = getRandomNum(field.top, field.height - mole.height)
                        y = getRandomNum(field.left, field.width - mole.width)
                        mole.right = y + mole.width
                        mole.bottom = x + mole.height
                        mole.top = x
                        mole.left = y
                    }
                    else
                    {
                        cancel()
                        saveTotalKills(moleKilled)
                        val result = Intent(this@MainActivity, Result::class.java)
                        result.putExtra(key, moleKilled)
                        startActivity(result)
                        finish()
                    }

                }
            }
            override fun onFinish() {
                val result = Intent(this@MainActivity, Result::class.java)
                result.putExtra(key, moleKilled)
                startActivity(result)
                finish()
            }
        }.start()

        menu.setOnClickListener(){
            val intent = Intent(this@MainActivity, Menu::class.java)
            startActivity(intent)
            finish()
        }
        mole.setOnClickListener()
        {
            mole.visibility = View.INVISIBLE
            moleKilled++
            moleCounter.text = "Убито кротов\n$moleKilled"
            mole.isEnabled = false
        }
    }

    fun getRandomNum(beg:Int, end: Int): Int
    {
        val diff = end - beg
        val random = Random()
        var i = random.nextInt(diff + 1)
        i += beg
        return i
    }
    fun saveTotalKills(res: Int)
    {
        val moleContainer = getSharedPreferences(Records.moleFile, Context.MODE_PRIVATE)
        val savedInt = moleContainer.getInt(Records.totalKillsKey,0)
        val ed = moleContainer.edit()
        ed.putInt(Records.totalKillsKey, savedInt + res).apply()
    }

    companion object {
        const val key = "com.example.zhenua.gameproject.key"
    }
}
