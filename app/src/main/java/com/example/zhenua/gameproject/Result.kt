package com.example.zhenua.gameproject

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_result.*

class Result : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        val result = intent.extras.getInt(MainActivity.key, 0)
        ourResult.text = result.toString()
        if(loadBestStreak() < result){
            newRecord.text = "Новый рекорд!"
            saveNewBestStreak(result)
        }
        else
        {
            newRecord.visibility = View.INVISIBLE
        }

        againButton.setOnClickListener()
        {
            val intent = Intent(this@Result, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        menuButton.setOnClickListener(){
            val intent = Intent(this@Result, Menu::class.java)
            startActivity(intent)
            finish()
        }

    }



    private fun loadBestStreak():Int
    {
        val moleContainer = getSharedPreferences(Records.moleFile, Context.MODE_PRIVATE)
        return moleContainer.getInt(Records.bestStreakKey,0)
    }
    private fun saveNewBestStreak(result: Int) {
        val moleContainer = getSharedPreferences(Records.moleFile, Context.MODE_PRIVATE)
        val newStreak = moleContainer.edit()
        newStreak.putInt(Records.bestStreakKey, result).apply()
    }

}
