package com.example.zhenua.gameproject

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_menu.*

class Menu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        gameBegin.setOnClickListener()
        {
            val intent = Intent(this@Menu, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        records.setOnClickListener()
        {
            val intent = Intent(this@Menu, Records::class.java)
            startActivity(intent)
        }

        Exit.setOnClickListener(){ finish() }

    }
}
